# Gestion bibliothèque

L'application a comme but d'aider à la gestion d'une petite bibliothèque, elle permet de 
consulter, ajouter et supprimer des livres. L'application a été réalisé en JAVA EE dans le cadre du
cours 635-1 donné par le professeur Nicolas Frankel à la Haute école de gestion de Genève.

### Prérequis
L'application nécessite un serveur Apache TomEE 7.0.47 pour être exécuté ainsi qu'un navigateur web afin d'effectuer les tests.
Pour pouvoir modifier le projet un éditeur de code ou un IDE est nécessaire.
Une connection à internet est nécessaire pour que les librairies (bootstrap) fonctionnent correctement.

Serveur web recommandé : [TomEE 7.0.47](http://tomee.apache.org/apache-tomee.html)
<br>
IDE recommandé  : [Intellij](https://www.jetbrains.com/idea/download)

### Installation

### Installation et configuration
1.- S'assurer que l'environnement d'exécution est en place et correctement configurer, si ce n'est pas déjà la cas il 
faut installer un serveur TomEE  versions 7.0.47  sur l'ordinateur ou le serveur en question 
(Il suffit de le télécharger puis de le deziper dans un répértoire que vous avez droit en 
lecture et écriture).

2.- Installer un IDE (par exemple Intellij IDEA) puis y configurer le serveur TomEE précédemment installé. 
Voici un tutoriel qui peut vous aider à configurer le serveur TomEE avec l'IDE Intellij: [tutoriel](https://formations.github.io/javaee/tp/servlet.html#_configuration_du_serveur_d_applications_2)

3.- Modifier le fichier tomcat-users.xml qui se trouve dans le dossier "conf" de votre serveur TomEE, rajouter les rôles
 ayant le droit de se connecter au serveur tomcat en rajoutant dans ```<tomcat-users>```  par exemple : 
```
<role rolename="tomee-admin" />
<user username="tomee" password="tomee" roles="tomee-admin,manager-gui"/>
<role rolename="MEMBER"/>
<user username"member" password="member" roles="MEMBER"/>
```
4.- L'application utilise par défaut la base de données HSQLDB du serveur TomEE (si vous modifier la source de données n'oubliez de changer tous les liens vers la base de données à l'étape 6)

5.- Créer les tables en executant sur le SGBD les lignes suivantes :
```
CREATE TABLE Books
(
    boo_id int PRIMARY KEY IDENTITY,
    boo_title varchar(100),
    boo_author varchar(100),
    boo_publisher varchar(50),
    boo_year int
);
CREATE INDEX boo__id_idx ON Books (boo_id);
```
6.- Rajouter le code suivant et modifier le lien JdbcUrl liant votre application à la base de données en modifiant 
le fichier tomee.xml qui se trouve dans le dossier conf du serveur TomEE (```../apache-tomee-webprofile-7.X.X/conf/tomee.xml```), 
remplacer le chemin par celui de votre base de données, par exemple : 
```
<Resource id="library" type="javax.sql.DataSource">
      JdbcDriver = org.hsqldb.jdbcDriver
      JdbcUrl = jdbc:hsqldb:file:/Users/votreUtilisateur/documents/votreProjet/database_hsqldb/library
      UserName = sa
</Resource>
```
ou ``` library ``` est le nom de la base de données, par la suite il faudra créer le dossier (```database_hsqldb ```) qui va contenir la
base de données, avant d'y mettre la base de données ce dossier doit être vide. Puis utiliser le programme hsqldb (qui se trouve dans les libraires de 
apache-tomee sous le nom ```hsqldb-x.x.x.jax ```) pour créer la base de données en introduisant dans le champs URL du programme hsqldb le chemin complet vers
la base de données avec le nom de cette dernière  (``` jdbc:hsqldb:file:/Users/votreUtilisateur/documents/votreProjet/database_hsqldb/library ``` par exemple)
## Vérification que l'application est fonctionnelle
Vérifier que l'application produit bel et bien le résultat attendu.
Pour effectuer un test il est nécessaire d'exécuter l'application sur le serveur TOMEE 

Procédure de test :
1. S'assurer que les étapes de l'installation (ci-dessus) ont toutes été effectués
2. Ouvrir le projet avec l'IDE 
3. Publier l'application en faisant un clique droit et puis en sélectionnant Publish.  
4. Lancer le serveur d’applications en faisant un clique droit sur le projet et sélectionner Debug.
5. Ouvrir un navigateur web de votre choix (par exemple Chrome) et taper l'adresse ```http://localhost:8080/votreServlet/```
6. L'application devrait s'afficher correctement via votre navigateur

## Déploiement
Pour déployer l'application il faut générer le fichier WAR de cette dernière.
Une fois obtenu le WAR, il faut déplacer le WAR vers le dossier ```webapp``` qui se trouve dans le dossier du serveur de TomEE.
## Built With
* [TomEE](http://tomee.apache.org/apache-tomee.html) - web serveur (envirronement d'exécution)
* [TOM EE](http://tomee.apache.org/apache-tomee.html) - web serveur (envirronement d'exécution)
* [Intellij](https://www.jetbrains.com/idea/download) - IDE / outil de développement
* Navigateur web pour vérification

## Versioning
Version 1.0.0 du 11 janvier 2019

## Auteurs

[Diego Ruiz](https://gitlab.com/diego1606) et [Aurélien Hamouti](https://gitlab.com/aurelienHamouti), étudiants en 3ème année de bachelor en informatique de gestion.


## License
Tout droits réservés en accord avec le réglement de la HEG Genève et la HES-SO

## Remerciement
Merci à Nicolas Frankel notre professeur pour son soutien
Merci également à la Haute école de gestion pour la mise é dispositon de son matériel pédagogique.