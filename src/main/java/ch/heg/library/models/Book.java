package ch.heg.library.models;

import javax.persistence.*;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="Books")
public class Book {
    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="boo_id")
    private long id;
    @Column(name = "boo_title")
    private String title;
    @Column(name = "boo_author")
    private String author;
    @Column(name = "boo_publisher")
    private String publisher;
    @Column(name = "boo_year")
    private int year;

    public Book() {}

    public Book(String title, String author, String publisher, int year) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.year = year;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String editor) {
        this.publisher = editor;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Book{" +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", year=" + year +
                '}';
    }
}
