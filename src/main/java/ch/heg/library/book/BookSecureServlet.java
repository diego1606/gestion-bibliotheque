package ch.heg.library.book;


import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.logging.Level;

public class BookSecureServlet extends HttpServlet {

    private static String PATH = "/WEB-INF/view/book/secure/new_book.jsp";

    @EJB
    BookManager manager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(PATH).forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(!isValidate(req)) throw new ServletException();
        manager.init(req);
        manager.save();
        resp.setHeader("Location",req.getContextPath()+"/books");
        resp.setContentType("text/html");
        resp.setStatus(HttpServletResponse.SC_CREATED);
        resp.sendError(301);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("id").isEmpty() ||! isValidate(req))throw new ServletException();
        try{
            int id = Integer.parseInt(req.getParameter("id"));
            manager.updateBook(id,req);
            resp.setStatus(HttpServletResponse.SC_OK);
        }catch (NumberFormatException nfe){
            resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }
    }
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            manager.deleteBook(id);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (NumberFormatException nfe){
            resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }

    }
    private boolean isValidate(HttpServletRequest req){
        if(req.getParameter("title").isEmpty() || req.getParameter("author").isEmpty() || req.getParameter("publisher").isEmpty() || req.getParameter("year").isEmpty()){
            return false;
        }
        return req.getParameter("year").length() == 4;
    }
}
