package ch.heg.library.book;

import ch.heg.library.models.Book;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.*;
import java.sql.PreparedStatement;
import java.util.List;

@Stateless
public class BookManager {

    private Book book;

    @PersistenceContext
    private EntityManager em;

    public BookManager() { }

    public void init(HttpServletRequest req){
        book = new Book(req.getParameter("title"), req.getParameter("author"), req.getParameter("publisher"),Integer.parseInt(req.getParameter("year")));
    }

    public List<Book> getBooks(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Book> cq = cb.createQuery(Book.class);
        cq.from(Book.class);
        TypedQuery<Book> q = em.createQuery(cq);
        return q.getResultList();
    }

    public void deleteBook(int id){
        Book book = em.find(Book.class,id);
        em.remove(book);
    }
    public void updateBook(int id, HttpServletRequest req){
        init(req);
        em.createQuery("UPDATE Book b " +
                "SET b.title=:title, b.author=:author, b.publisher=:publisher, b.year=:year" +
                " WHERE b.id=:id")
                .setParameter("title",book.getTitle())
                .setParameter("author",book.getAuthor())
                .setParameter("publisher",book.getPublisher())
                .setParameter("year",book.getYear())
                .setParameter("id",id).executeUpdate();
        /*
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaUpdate<Book> update = cb.createCriteriaUpdate(Book.class);
        Root bookRoot = update.from(Book.class);
        update.set(bookRoot.get("title"),book.getTitle())
                .set(bookRoot.get("author"),book.getAuthor())
                .set(bookRoot.get("publisher"),book.getPublisher())
                .set(bookRoot.get("year"),book.getYear())
                .where(cb.equal(bookRoot.get("id"),id));
        em.createQuery(update).executeUpdate();*/
    }
    @Transactional(rollbackOn = Exception.class)
    public void save() {
        em.persist(book);
    }
}
