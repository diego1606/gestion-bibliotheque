package ch.heg.library.book;


import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class IndexServlet extends HttpServlet {
    private static String PATH = "/WEB-INF/view/book/index.jsp";
    private static String BOOKS_FILD = "books";
    @EJB
    BookManager manager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute(BOOKS_FILD,manager.getBooks());
        req.getRequestDispatcher(PATH).forward(req,resp);
    }
}
