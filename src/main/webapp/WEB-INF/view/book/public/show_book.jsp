<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<tr>
    <th scope="row">${param.title}</th>
    <td>${param.author}</td>
    <td>${param.publisher}</td>
    <td>${param.year}</td>
</tr>
