<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <title>App bibliothèque</title>
</head>
<body>
<%@ include file="../../layout/header.jsp" %> <%--&lt;%&ndash; The header section &ndash;%&gt;--%>

<div class="container">
<br>
   <form action="${pageContext.request.contextPath}/secure/new" method="post" id="form-new-book">
       <div class="form-group row">
           <label for="inp_title" class="col-sm-2 col-form-label">Titre<small>*</small> : </label>
           <div class="col-sm-10">
               <input type="text" class="form-control" id="inp_title" name="title">
           </div>
       </div>

       <div class="form-group row">
           <label for="inp_author" class="col-sm-2 col-form-label">Auteur<small>*</small>  : </label>
           <div class="col-sm-10">
               <input type="text" class="form-control" id="inp_author" name="author">
           </div>
       </div>

       <div class="form-group row">
           <label for="inp_publisher" class="col-sm-2 col-form-label">Editeur<small>*</small>  : </label>
           <div class="col-sm-10">
               <input type="text" class="form-control" id="inp_publisher" name="publisher">
           </div>
       </div>

       <div class="form-group row">
           <label for="inp_year" class="col-sm-2 col-form-label">Année<small>*</small>  : </label>
           <div class="col-sm-10">
               <input type="number" class="form-control" id="inp_year" name="year" min="0" max="2019">
           </div>
       </div>
       <div class="row">
           <div class="col-12">
               <small>* les champs sont obligatoire</small>
           </div>
       </div>
       <div class="form-group row">
           <div class="offset-3 col-9">
               <button type="submit" class="btn btn-primary">Créer</button>
           </div>
       </div>
   </form>
</div><%-- The body section --%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script type="application/javascript">
    <%@ include file="../../../../script/scripts.js" %>
</script>
</body>
</html>