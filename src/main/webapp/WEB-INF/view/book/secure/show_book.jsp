<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<tr id="book-${param.id}">
    <td scope="row">
        <input type="text" name="title" class="form-control" placeholder="${param.title}" id="book-title-${param.id}" value="${param.title}">
    </td>
    <td>
        <input type="text" name="author" class="form-control" placeholder="${param.author}" id="book-author-${param.id}" value="${param.author}">
    </td>
    <td>
        <input type="text" name="publisher" class="form-control" placeholder="${param.publisher}" id="book-publisher-${param.id}" value="${param.publisher}">
    </td>
    <td>
        <input type="number" min="0" max="2019" class="form-control" placeholder="${param.year}" id="book-year-${param.id}" value="${param.year}">
    </td>
    <td>
        <div class="row">
            <div class="col-6"><button type="button" class="btn btn-light btn-update-book" data-id="${param.id}">Modier</button></div>
            <div class="col-6"><button type="button" class="btn btn-danger btn-delete-book" data-id="${param.id}">Supprimer</button></div>
        </div>

    </td>
</tr>

