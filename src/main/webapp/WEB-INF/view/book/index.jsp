<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="ch.heg.library.models.Book" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="ch.heg.library.IndexServlet" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>App bibliothèque</title>
</head>
<body>
<%@ include file="../layout/header.jsp" %> <%-- The header section --%>
<div class="container">
    <div id="msg-event"></div>
    <div class="row">
        <div class="col-12">
            <table class="table table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">Titre</th>
                        <th scope="col">Auteur(s)</th>
                        <th scope="col">Editeur</th>
                        <th scope="col">Année</th>
                    </tr>
                </thead>
                <tbody>
                <%
                    Iterator<Book> iterator = ((List<Book>)request.getAttribute("books")).iterator();
                    Book book = null;
                    if(iterator.hasNext()){
                        if (request.getRemoteUser() == null){
                            while (iterator.hasNext()){
                                book = iterator.next();%>
                                <jsp:include page="public/show_book.jsp">
                                    <jsp:param name="title" value="<%=book.getTitle()%>"/>
                                    <jsp:param name="author" value="<%=book.getAuthor()%>"/>
                                    <jsp:param name="publisher" value="<%=book.getPublisher()%>"/>
                                    <jsp:param name="year" value="<%=book.getYear()%>"/>
                                </jsp:include><%
                            }
                        } else {
                            while (iterator.hasNext()){
                                book = iterator.next();%>
                                <jsp:include page="secure/show_book.jsp">
                                    <jsp:param name="id" value="<%=book.getId()%>"/>
                                    <jsp:param name="title" value="<%=book.getTitle()%>"/>
                                    <jsp:param name="author" value="<%=book.getAuthor()%>"/>
                                    <jsp:param name="publisher" value="<%=book.getPublisher()%>"/>
                                    <jsp:param name="year" value="<%=book.getYear()%>"/>
                                </jsp:include><%
                            }
                        }
                    } else {%>
                        <tr>
                            <th colspan="4">
                                Aucun libre
                            </th>
                        </tr>
                    <%}
                 %>
                </tbody>
            </table>
        </div>
    </div>
    <%if (request.getRemoteUser() != null){%>
        <div class="row">
            <div class="col-12 ">
                <a class="btn btn-primary float-right" href="secure/new">
                    Nouveau
                </a>
            </div>
        </div>
    <%}%>
</div><%-- The body section --%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script type="application/javascript">
    <%@ include file="../../../script/scripts.js" %>
</script>
</body>
</html>