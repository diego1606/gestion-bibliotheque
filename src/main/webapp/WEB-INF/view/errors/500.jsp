<%--
  Created by IntelliJ IDEA.
  User: diegoruiz
  Date: 13.12.18
  Time: 21:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true"%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>App bibliothèque | 500</title>
</head>
<body>
<%@ include file="../layout/header.jsp" %> <%-- The header section --%>
<div class="container">
    <div class="row">
        <div class="col-12 text-center text-uppercase">
            <h1>HTTP 500 PAGE - Internal Server Error</h1>
        </div>
        <div class="col-12 text-center">
            <p>
                Le serveur a rencontré une erreur interne.
            </p>
        </div>
    </div>
</div>
</body>
</html>
