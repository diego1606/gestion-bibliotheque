<%--
  Created by IntelliJ IDEA.
  User: diegoruiz
  Date: 07.12.18
  Time: 11:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true"%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>App bibliothèque | Not found</title>
</head>
<body>
    <%@ include file="../layout/header.jsp" %> <%-- The header section --%>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Page not found</h1>
            </div>
        </div>
    </div>
</body>
</html>
