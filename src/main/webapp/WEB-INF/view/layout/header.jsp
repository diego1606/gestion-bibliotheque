<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<nav class="navbar navbar-light bg-light">
    <div class="col-12 col-sm-3">
        <div class="navbar-brand">
            <a href="${pageContext.request.contextPath}/" data-url="${pageContext.request.contextPath}" id="name-application">
                App bibliothèque
            </a>
        </div>
    </div>
    <div class="col-6 col-sm-3 text-center">
        <a href="<%=request.getContextPath() + "/books"%>">Livres</a>
    </div>
    <div class="col-6 col-sm-3 ">
        <%if(request.getRemoteUser() == null){
            if(!request.getRequestURL().toString().toLowerCase().contains("login.jsp")){%>
                <a class='btn btn-light float-right' href='${pageContext.request.contextPath}/secure/index'>Connexion</a>
           <% }
        } else { %>
            <a class='btn btn-light float-right' href='${pageContext.request.contextPath}/secure/logout'>Déconnexion</a>
        <%}%>
    </div>
</nav>
