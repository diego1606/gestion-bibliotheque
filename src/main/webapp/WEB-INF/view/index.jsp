<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>App bibliothèque</title>
</head>
<body>
<%@ include file="layout/header.jsp" %> <%-- The header section --%>

<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-6">
            <div class="row">
                <div class="col-12">
                    <h1> Bienvenu dans votre bibliothèque virtuelle </h1>
                </div>
                <div class="col-12">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi esse et ipsum magni nostrum quaerat quia Delectus magnam molestias mollitia omnis quis quo quos ullam. At enim eum ipsa suscipit.
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <img class="img-fluid" alt="library" src="http://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/bibliotheque.jpg?itok=AnndQaIa">
        </div>
    </div>
</div><%-- The body section --%>
</body>
</html>