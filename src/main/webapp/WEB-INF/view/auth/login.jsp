<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>App bibliothèque</title>
</head>
<body>
<%@ include file="../layout/header.jsp" %> <%-- The header section --%>
<div class="container">
  <br>
    <form action="j_security_check" method="post">
        <div class="form-group row">
            <label for="inp_username" class="col-sm-2 col-form-label">Identifiant : </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inp_username" name="j_username">
            </div>
        </div>

        <div class="form-group row">
            <label for="inp_password" class="col-sm-2 col-form-label">Mot de passe : </label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="inp_password" name="j_password">
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-3 col-9">
                <button type="submit" class="btn btn-primary">Se connecter</button>
            </div>
        </div>

    </form>
</div><%-- The body section --%>
</body>
</html>