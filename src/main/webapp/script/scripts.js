$(document).ready(function(){
    $('.btn-delete-book').on('click',function (e) {
        var id = $(e.target).data('id');
        $.ajax({
            url: $('#name-application').data('url')+'/secure/book/delete?id='+id,
            type: "DELETE",
            success:function (reponse, status) {
                if(status === "success"){
                    $('#book-'+id).remove();
                    $('#msg-event').append('<div class="alert alert-success" role="alert">Vous avez supprimé le livre avec succès</div>')
                }
            },
            error: function () {
                $('#msg-event').append(msgError());
            },
            complete: function(){
                setTimeout(function(){ $('#msg-event').empty(); }, 4000);
            }
        })
    });
    $('.btn-update-book').on('click',function (e) {
        var id = $(e.target).data('id');
        var title = $('#book-title-'+id).val();
        var author = $('#book-author-'+id).val();
        var publisher = $('#book-publisher-'+id).val();
        var year = $('#book-year-'+id).val();
        $.ajax({
            url: $('#name-application').data('url')+'/secure/book?id='+id+'&title='+title+'&author='+author+'&publisher='+publisher+'&year='+year,
            type: "PUT",
            //data:{id:id, title:title, author:author, publisher:publisher, year:year},
            success:function (reponse, status) {
                if(status === 'success'){
                    $('#book-title-'+id).val(title);
                    $('#book-author-'+id).val(author);
                    $('#book-publisher-'+id).val(publisher);
                    $('#book-year-'+id).val(year);
                    $('#book-title-'+id).attr('placeholder',title);
                    $('#book-author-'+id).attr('placeholder',author);
                    $('#book-publisher-'+id).attr('placeholder',publisher);
                    $('#book-year-'+id).attr('placeholder',year);
                    $('#msg-event').append('<div class="alert alert-success" role="alert">La mise à jour du livre a été enregistré.</div>')
                }
            },
            error:function () {
                $('#msg-event').append(msgError());
            },
            complete:function () {
                setTimeout(function(){ $('#msg-event').empty(); }, 4000);
            }
        })
    });
    $('#form-new-book').validate({
        rules:{
            title: "required",
            author: "required",
            publisher: "required",
            year:{
                required: true,
                minlength: 4
            }
        },
        messages : {
            title: "Le champ Titre est obligatoire.",
            author: "Le champ Auteur est obligatoire.",
            publisher: "Le champ Editeur est obligatoire.",
            year:{
                required: "Le champ Année est obligatoire.",
                minlength: "Le champ Années doit contenir 4 chiffres."
            }
        },
        errorElement : 'small',
        errorClass : 'text-danger',
        submitHandler: function(form) {
            $('#form-new-book').submit();
        }
    })
    function msgError() {
        return '<div class="alert alert-danger" role="alert">Une erreur s\'est produite. Veuillez réessayer\n</div>'
    }
});
